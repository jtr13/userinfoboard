# The Data

The information board automatically collects its data from this directory and any information added to the files contained here will be rendered on the information board once the `index.Rmd` file is rendered into a HTML file.

The table below describes the files within this data directory and the years for which each dataset covers.

| Data File | Description | Summary |
| ---	    | ---	  | ---	    |
| conferences.csv | useR! conference location data | 2004 - 2022 |
| key_dates.csv | information about key dates | 2016 - 2024 |
| keynotes.csv | useR! keynotes | 2004 - 2022 |
| organizing_committee_members.csv | organizing committee member information | 2016 - 2022 |
| modified_gender.csv | gender information of participants | 2016 - 2020 |
| participant_numbers.csv | count of participants | 2016 - 2021 |
| participation_country.csv | countries of participants | 2016 - 2021 |
| program.csv | useR! Program data | 2016 - 2021 |
| program_committee_members.csv | program committee member information | 2004 - 2022 |
| registration_types.csv | count of participants by profession | 2016 - 2021 |
| registration_fees.csv | registration fees | 2016-2019, 2021 |
| sponsors.csv | data on useR! sponsors | 2014 - 2022 |
| tutorials.csv | useR! tutorials | 2017 - 2021 |

## Dates

Key dates are in ISO8601 format, i.e., "YYYY-MM-DD". Opening in Excel may 
change the format - either format the cells with the Custom format "yyyy-mm-dd" 
before saving the CSV, or use an editor that keeps the format, like 
[Modern CSV](https://www.moderncsv.com/). 

## Notes for specific data files

### Key dates

Please map to one of previously defined options, or adapt to use the same key words/phrases:

|Action                                                                                             |
|:--------------------------------------------------------------------------------------------------|
|Abstract selection and diversity scholarship notifications                                         |
|Abstract selection notifications                                                                   |
|Abstract submission and diversity scholarship application closes                                   |
|Abstract submission closes                                                                         |
|Abstract submission opens                                                                          |
|Abstract submission opens for regular talks, panels, and posters                                   |
|Abstract submission opens for regular talks, panels, incubators and elevator pitches               |
|Abstract submission opens for tutorials and presentations; diversity scholarship application opens |
|ASA grant application closes                                                                       |
|Conference closes                                                                                  |
|Conference opens                                                                                   |
|Datathon entry closes                                                                              |
|Deadline for additional AV requests                                                                |
|Deadline for selected contributors to send posters and optional elevator pitch                     |
|Deadline for selected contributors to send us videos (virtual only)                                |
|Deadline for selected contributors to send us videos, technical notes and transcripts              |
|Deadline for slides                                                                                |
|Diversity scholarship application closes                                                           |
|Diversity/needs-based scholarship application closes                                               |
|Diversity/needs-based scholarship application opens                                                |
|Early bird rates are extended until registration closes                                            |
|Early bird registration closes                                                                     |
|Late registration closes                                                                           |
|Poster submission closes                                                                           |
|Registration closes                                                                                |
|Registration opens                                                                                 |
|Regular registration closes                                                                        |
|Schedule announced                                                                                 |
|Tutorial day                                                                                       |
|Tutorial selection notifications                                                                   |
|Tutorial submission closes                                                                         |
|Tutorial submission opens                                                                          |
|Website online                                                                                     |
|Young academics scholarship application closes                                                     |

## License

The data is provided under a [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/) license

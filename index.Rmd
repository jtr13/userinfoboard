---
title: "Information Board"
output: 
  flexdashboard::flex_dashboard:
    orientation: rows
    vertical_layout: scroll
    source_code: "https://gitlab.com/rconf/userinfoboard"
    self_contained: false
    includes:
      in_header: "includes/beforeInit.html"
      after_body: ["includes/footer.html", "includes/afterInit.html"]
    css: ["includes/custom.css", "css/custom.css"]
    logo: assets/user-logo.png
---

```{r setup, include=FALSE}
pkgload::load_all(".")
```

Sidebar {.sidebar data-width=200}
=====================================

```{r, echo=FALSE, results='asis'}
htmltools::includeHTML('includes/menu.html')
```

Welcome
=====================================

Row {data-height=650}
-----------------------------------------------------------------------

### Welcome 

Welcome to the useR! information board <i class="fa fa-home"></i>

The useR! conferences are non-profit international events organized by community volunteers for the R community, and supported by the R Foundation. They are the main meeting for the R user and developer community. The useR! program consists of both invited and user contributed presentations. Attendees include R developers and users, who are data scientists, business intelligence specialists, analysts, statisticians, academics, and students.

This information board provides a way to find resources, facts and figures on past useR! Conferences. The information can help you explore the history of the the R community and their achievements, or if you are a conference organizer yourself, give you insight into how past events were organized, and hopefully inform/support your work on future global, regional or local R events.
 
The information board gathers data about the conferences' programs, participants, venues, locations, speakers, fees, and sponsors. To find even more information about past useR! events (e.g. blog posts, conference photos), please visit the [R Project's Conferences page](https://www.r-project.org/conferences/), which has links to the conference websites for each year. In addition, you can check out our sister project, [The useR! Knowledgebase](https://rconf.gitlab.io/userknowledgebase), an online documentation project for useR! Organizers containing resources, learning, and guides, based on previous organizers work and experiences. 


![Past useR! Conferences](./assets/user-conf.png)

**Photos Credits:** Dale Maschette @Dale_Masch

Row
-----------------------------------------------------------------------


Overview
=====================================

Row
-------------------------------------

### useR! Conferences

```{r}
valueBox(conf_events_total, icon = "fa-calendar", color = "white")
```


### Cities

```{r}
valueBox(conf_city_total, icon = "fa-map-marker", color = "white")
```

### Countries

```{r}
valueBox(conf_country_total, icon = "fa-globe", color = "white")
```

### Sponsors

```{r}
valueBox(sponsor_count, icon = "fa-dollar", color = "white")
```

Row {data-height=600}
-----------------------------------------------------------------------

### Past useR! Conference Locations

```{r}
leaflet_map(conferences)
```

### Past useR! Conference Distribution

```{r}
conf_countries_heatmap()
```


Row {data-height=400}
-----------------------------------------------------------------------

### Countries

```{r}
conf_countries_table()
```

### Regions

```{r}
conf_regions_table()
```

Organizational Information
=====================================

Row {data-height=400}
-----------------------------------------------------------------------

### Sponsors

```{r}
sponsors_table()
```

### Sponsors Per Year

```{r}
sponsors_per_year_table()
```

Row {data-height=600}
-----------------------------------------------------------------------

### Sponsors on the Map
```{r}
sponsor_map(sponsors_count)

```

Row {data-height=400}
-----------------------------------------------------------------------

### Key Dates
```{r}
key_dates_table()
```

### Registration Fees
```{r}
registration_fees_table()
```

Row {data-height=400}
-----------------------------------------------------------------------

### Organizing Committee Members {data-width=400}

```{r}
organizing_committee_table()
```

### Organizing Committee Member Frequency {data-width=400}

```{r}
organizing_committee_frequency()
```

Programs
=====================================

Row {data-height=400}
-----------------------------------------------------------------------

### Programs {data-width=800}

```{r}
programs_table()
```

Row {data-height=400}
-----------------------------------------------------------------------

### Distribution of Presentation {data-width=700}

```{r}
program_bar_chart()
```

Row {data-height=400}
-----------------------------------------------------------------------

### Tutorials {data-width=700}

```{r}
tutorial_table()
```

### Tutorials Per Year {data-width=300}

```{r}
tutorials_per_year()
```


Row {data-height=400}
-----------------------------------------------------------------------

### Keynote Word Cloud {data-width=400}

```{r}

keynotes_wordcloud(keynotes)

```


### Tutorial Word Cloud {data-width=400}

```{r}

tutorial_wordcloud(tutorials)

```

Row {data-height=400}
-----------------------------------------------------------------------


### Keynotes {data-width=700}

```{r}
keynote_speakers()
```

### Keynotes Per Year {data-width=300}

```{r}
keynotes_per_year()
```


Row {data-height=400}
-----------------------------------------------------------------------

### Program Committee Members {data-width=400}

```{r}
program_committee_table()
```

### Program Committee Member Frequency {data-width=400}

```{r}
program_committee_frequency()
```



Participation
=====================================

Row {data-height=400}
-----------------------------------------------------------------------

### World View of useR! Participation {data-width=800}

```{r}
world_view()
```

Row {data-height=400}
-----------------------------------------------------------------------

### Countries grouped by Continent {data-width=600}

```{r}
continent_view()
```

### Countries grouped by Income Level {data-width=600}

```{r}
income_view()
```


Row {data-height=400}
-----------------------------------------------------------------------

### Professional Levels {data-width=600}


```{r}
professional_levels_chart()
```

### Participants {data-width=600}


```{r}
participant_numbers_chart()
```

Row {data-height=400}
-----------------------------------------------------------------------
### Gender {data-width=600}


```{r}
gender_chart()
```
![*For privacy reasons, an arbitrary number of 2.5 is used where the count is 5 or below](./)


### Gender Percentage {data-width=600}


```{r}
gender_percent_chart()
```

Row {data-height=400}
-----------------------------------------------------------------------
### Ethnicity {data-width=600}

```{r}
ethnicity_chart()
```

![*For privacy reasons, an arbitrary number of 2.5 is used where the count is 5 or below](./)

```{js}

```